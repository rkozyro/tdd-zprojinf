<?php
include('../index.php');
 
class palindromTest extends PHPUnit_Framework_TestCase {

    public function testPalindroms()
    {
        $Palindroms = array(1001, 123321, 12321, 9009, 77, 1771);
        $maxPalindromProduct= new biggest3DigitPalindromProduct();
        foreach($Palindroms as $number)
        {
          $value = $maxPalindromProduct->isPalindrom($number);
          $this->assertTrue($value, 'isPalindrom('.$number.') powinno zwrocic true :(');
        }
    }

}
?>