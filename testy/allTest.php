 <?php
include('../index.php');


class allTest extends PHPUnit_Framework_TestCase {
 
     public function testNotPalindroms()
    {
        $notPalindroms = array(1234, 1000, 724, 500);
        $maxPalindromProduct = new biggest3DigitPalindromProduct();
        foreach($notPalindroms as $number)
        {
          $value = $maxPalindromProduct->isPalindrom($number);
          $this->assertFalse($value, 'isPalindrom('.$number.') powinno zwrocic false :(');
        }
    }
    
    public function testPalindroms()
    {
        $Palindroms = array(1001, 123321, 12321, 9009, 77, 1771);
        $maxPalindromProduct= new biggest3DigitPalindromProduct();
        foreach($Palindroms as $number)
        {
          $value = $maxPalindromProduct->isPalindrom($number);
          $this->assertTrue($value, 'isPalindrom('.$number.') powinno zwrocic true :(');
        }
    } 
	
    public function testBiggest()
    {
        $maxPalindromProduct= new biggest3DigitPalindromProduct();
        $value = $maxPalindromProduct->getBiggest();
        $this->assertEquals($value, 906609, 'najwiekszy palindrom powinien wynosic 906609 :(');
    }
 	
}
?>