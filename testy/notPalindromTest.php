<?php
include('../index.php');
 
class notPalindromTest extends PHPUnit_Framework_TestCase {
 
    
    public function testNotPalindroms()
    {
        $notPalindroms = array(1234, 1000, 724, 500, 600, 120);
        $maxPalindromProduct = new biggest3DigitPalindromProduct();
        foreach($notPalindroms as $number)
        {
          $value = $maxPalindromProduct->isPalindrom($number);
          $this->assertFalse($value, 'isPalindrom('.$number.') powinno zwrocic false :(');
        }
    }
 
}
?>