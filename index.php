<?php
/*
Rados�aw Kozyro 13K3 (tydzie� parzysty)
Lab TDD
Biblioteka u�yta do test�w jednostkowych: PHPUnit

Zadanie nr 4
Largest palindrome product (two 3 digit numbers)
*/

  class biggest3DigitPalindromProduct
  {

    /*

    Public: check if given $number is palindrom

    $number The number to be checked

    Example

    isPalindrom(1234) => false

    isPalindrom(1771) => true

    Returns 'true' if given number is palindrom otherwise returns 'else'
    */
	public function isPalindrom($number)
    {
        return (strval($number) === strrev(strval($number)));
    }

    public function getBiggest()
    {
      $biggestPalindrome = 0;
      for($a = 100; $a < 1000; $a++)
      {
        for($b = 100; $b < 1000; $b++)
        {
		  $product = $a * $b;
          if($this->isPalindrom( $product) && ( $product > $biggestPalindrome))
            $biggestPalindrome =  $product;
        }
      }
      return $biggestPalindrome;
    }
	
  }

  
$maxPalindrom = new biggest3DigitPalindromProduct();
echo $maxPalindrom->getBiggest()."\n";
  
?>